/* This program is based on the simple hibernate example used for CIT-360 written by
   Troy Tuckett (BYUI.EDU), available at the following URL:
   https://bitbucket.org/tuckettt/hibernatetest/src/master/src/edu/byui/tuckett/hibernatetest/TestDAO.java
   This class has been written based on the above mentioned example following an online lecture on youtube
   that demonstrates the basics of utilizing hibernate to establish an SQL database connection using a
   singleton. More functionality has been added. */

package mshort.week09;

import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.*;
import java.util.concurrent.TimeUnit;

/* AccessCarsDB implemented using a singleton
   Used to get customer data from my MYSQL database */
public class AccessCarsDB {

    SessionFactory factory;
    Session session;

    private static AccessCarsDB single_instance = null;

    private AccessCarsDB()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /* Establish singleton model. This maintains only one instance of the
       class, creating it the first time in one does not exists */
    public static AccessCarsDB getInstance()
    {
        if (single_instance == null) {
            single_instance = new AccessCarsDB();
        }

        return single_instance;
    }

    /*  Used to get more than one customer from database
        Uses the OpenSession construct rather than the
        getCurrentSession method so that I control the
        session.  Need to close the session myself in finally. */
    public List<UsedCar> getUsedCars() {

        try {
            session = factory.openSession();
            //session.setCacheMode(CacheMode.IGNORE);
            session.clear();
            session.getTransaction().begin();
            String sql = "from mshort.week09.UsedCar";
            List<UsedCar> ucs = (List<UsedCar>) session.createQuery(sql).getResultList();
            //session.clear();
            session.getTransaction().commit();
            //session.flush();
            return ucs;

        } catch (Exception e) {
            System.out.println("Exception encountered in: getUsedCars");
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /* Used to get a single used car from database */
    public UsedCar getUsedCar(int id) {

        try {
            session = factory.openSession();
            //session.setCacheMode(CacheMode.IGNORE);
            session.clear();
            session.getTransaction().begin();
            String sql = "from mshort.week09.UsedCar where id=" + id;
            UsedCar uc = (UsedCar)session.createQuery(sql).getSingleResult();
            //session.clear();
            session.getTransaction().commit();
            //session.flush();
            return uc;

        } catch (Exception e) {
            System.out.println("Exception encountered in: getUsedCar");
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    /* Used to insert a single UsedCar into database */
    public void addUsedCar(UsedCar inUsedCar) {

        try {
            session = factory.openSession();
            //session.setCacheMode(CacheMode.IGNORE);
            //session.clear();
            Transaction t = session.getTransaction();
            t.begin();
            //session.getTransaction().begin();
            session.saveOrUpdate(inUsedCar);
            t.commit();
            //session.clear();
            //session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("Exception encountered in: addUsedCar");
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
        } finally {
            session.close();
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /* TODO: Insert Multiple Using List if Time Permits */

}