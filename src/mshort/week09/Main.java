package mshort.week09;

import java.util.List;

public class Main
{
    public static void main(String[] args) {

        /* get instance of AccessCarsDB */
        AccessCarsDB myAccessCarsDBSession = AccessCarsDB.getInstance();

        System.out.println("RETRIEVE DATA");
        /* get UsedCar with id of 1 from DB and print object contents */
        UsedCar myUsedCar = myAccessCarsDBSession.getUsedCar(1);
        System.out.println("*** Select Row with id of 1 into UsedCar object ***");
        System.out.println(myUsedCar.toString() + "\n");

        /* Print out all */
        List<UsedCar> ucList = myAccessCarsDBSession.getUsedCars();
        System.out.println("*** Select All Rows and output List of UsedCar objects ***");
        for (UsedCar myCar : ucList) {
            System.out.println(myCar);
        }

        System.out.println("ADD DATA");
        /* Create a UsedCar Object and add it to the database */
        UsedCar myOldCar = new UsedCar();
        myOldCar.setColor("Red");
        myOldCar.setMake("Volkswagen");
        myOldCar.setModel("Jetta");
        myOldCar.setYear(2012);
        myOldCar.setMilage(521389);
        myAccessCarsDBSession.addUsedCar(myOldCar);
        System.out.println(myOldCar.toString());
        System.out.println();

        System.out.println("RETRIEVE DATA");
        System.out.println("*** Select All Rows and output List of UsedCar objects ***");
        for (UsedCar myCar : ucList) {
            System.out.println(myCar);
        }
    }


}
