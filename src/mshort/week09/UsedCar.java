package mshort.week09;

import javax.persistence.*;

@Entity
@Table(name = "used")
public class UsedCar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "make")
    private String make;

    @Column(name = "model")
    private String model;

    @Column(name = "year")
    private int year;

    @Column(name = "color")
    private String color;

    @Column(name = "milage")
    private int milage;

    /* Do we need this setter? Creation should have it as undefined, adding it to the DB should auto increment */
    /* Will Test this */
    public void setId(int inId) {
        this.id = inId;
    }
    public int getId() {
        return id;
    }

    public void setMake(String inMake) {
        this.make = inMake;
    }
    public String getMake() {
        return make;
    }

    public void setModel(String inModel) {
        this.model = inModel;
    }
    public String getModel() {
        return model;
    }

    public void setYear(int inYear) {
        this.year = inYear;
    }
    public int getYear() {
        return year;
    }

    public void setColor(String inColor) {
        this.color = inColor;
    }
    public String getColor() {
        return color;
    }

    public void setMilage(int inMilage) {
        this.milage = inMilage;
    }
    public int getMilage() {
        return milage;
    }




    public String toString() {
        return "Id: " + id + " Year: " + year + ", Make: " + make + " , Model: " + model + " , Color: " + color + ", Milage: " + milage;
    }

}
